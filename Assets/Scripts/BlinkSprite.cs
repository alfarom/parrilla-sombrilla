﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkSprite : MonoBehaviour {

	public SpriteRenderer spriteRenderer;
	public bool lockRotation = true;
	private void OnEnable()
	{
		if (!lockRotation)
			transform.LookAt(GameObject.FindGameObjectWithTag("MainCamera").transform, Vector3.up);
		StartCoroutine("Blink");
	}

	private void OnDisable()
	{
		StopCoroutine("Blink");
	}

	IEnumerator Blink()
	{
		while (true)
		{
			spriteRenderer.enabled = !spriteRenderer.enabled;
			yield return new WaitForSeconds(0.3f);
		}

	}
}
