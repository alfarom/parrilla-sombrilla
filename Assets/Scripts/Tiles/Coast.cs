﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coast: MonoBehaviour
{

    public int row { get; set; }
    public int column { get; set; }
}
