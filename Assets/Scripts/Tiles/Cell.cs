﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour {

	public int row { get; set; }
	public int column { get; set; }
	public bool isFree {
		get { return transform.childCount == 0; }
	}
}
