﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiriFXAudio : MonoBehaviour {

	public AudioSource audioSource;

	public AudioClip guiriToThePoint;
	public AudioClip flop;
	public AudioClip guiriScorched;

	public AudioClip[] tanAudios;
	public AudioClip[] burnedAudios;
	public AudioClip[] scorchedAudios;


	public void OnGuiriToThePoint()
	{
		audioSource.Stop();
		audioSource.PlayOneShot(guiriToThePoint);
	}

	public void OnFlop()
	{
		audioSource.Stop();
		audioSource.PlayOneShot(flop);
	}

	public void OnGuiriScorched()
	{
		audioSource.Stop();
		audioSource.PlayOneShot(guiriScorched);
	}

	public void OnTanGuiri()
	{
		audioSource.Stop();
		audioSource.PlayOneShot(tanAudios[Random.Range(0, tanAudios.Length)]);
	}

	public void OnBurnedGuiri()
	{
		audioSource.Stop();
		audioSource.PlayOneShot(burnedAudios[Random.Range(0, burnedAudios.Length)]);
	}

	public void OnScorchedGuiri()
	{
		audioSource.Stop();
		audioSource.PlayOneShot(scorchedAudios[Random.Range(0, scorchedAudios.Length)]);
	}
}
