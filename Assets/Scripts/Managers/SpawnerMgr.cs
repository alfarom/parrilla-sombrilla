﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerMgr : MonoBehaviour
{

    [Tooltip("Tiempo para que se añadan nuevas filas")]
    public float _timeToAddNewTourist = 2f;

    public GameObject[] tourists;

    private GameObject freeCell;

    private int touristPosibilidad = 0;
    private int spawnPointsPosibilidad = 0;

    //Tiempo que queda la siguiente subida de nivel
    private float timeLeft;
    private Coroutine timerCoroutine;

    public void Start()
    {
		timerCoroutine = StartCoroutine(CountDownLevel(_timeToAddNewTourist));
    }


    IEnumerator CountDownLevel(float timeLeft)
    {

        while (timeLeft > 0)
        {
            yield return null;
            timeLeft -= Time.deltaTime;
            this.timeLeft = timeLeft;
        }
        AddNewTourist();
        timerCoroutine = StartCoroutine(CountDownLevel(_timeToAddNewTourist));
    }


    private void OnEnable()
    {
        if (timeLeft > 0)
            timerCoroutine = StartCoroutine(CountDownLevel(timeLeft));
    }

    private void OnDisable()
    {
        if (timerCoroutine != null)
        {
            StopCoroutine(timerCoroutine);
            timerCoroutine = null;
        }
    }

    private void AddNewTourist()
    {
        touristPosibilidad = Random.Range(0, tourists.Length);
        spawnPointsPosibilidad = Random.Range(0, GameMgr.Instance.spawnPoints.Length);

        freeCell = GameMgr.Instance.grid.GetFreeCell();

        GameObject tourist = Instantiate(tourists[touristPosibilidad], GameMgr.Instance.spawnPoints[spawnPointsPosibilidad].transform);
        tourist.transform.localScale = Vector3.one;
        tourist.transform.localPosition = Vector3.zero;
        tourist.transform.parent = freeCell.transform;
    }
}
