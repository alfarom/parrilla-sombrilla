﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerMgr : MonoBehaviour {

	[Tooltip("Tiempo para que se añadan nuevas filas")]
	public float _timeToAddNewLevel=20f;

	//Tiempo que queda la siguiente subida de nivel
	private float timeLeft;
	private Coroutine timerCoroutine;

	public void Start()
	{
		timerCoroutine = StartCoroutine(CountDownLevel(_timeToAddNewLevel));
	}


	IEnumerator CountDownLevel(float timeLeft)
	{
		
		while (timeLeft>0)
		{
			yield return null;
			timeLeft -= Time.deltaTime;
			this.timeLeft = timeLeft;
        }

        GameMgr.Instance.spawnPointsPosition.transform.position += Vector3.up;
        GameMgr.Instance.camera.Objetivo = GameMgr.Instance.camera.transform.position + new Vector3(0, 0.7f, -1.1f);
        GameMgr.Instance.camera.cameraInPosition = false;

		GameMgr.Instance.grid.AddNewLevel();
		if(GameMgr.Instance.grid.levels == 7){
			Destroy(this);
		}
		timerCoroutine = StartCoroutine(CountDownLevel(_timeToAddNewLevel));
	}


	private void OnEnable()
	{
		if(timeLeft>0)
			timerCoroutine = StartCoroutine(CountDownLevel(timeLeft));
	}

	private void OnDisable()
	{
		if (timerCoroutine != null)
		{
			StopCoroutine(timerCoroutine);
			timerCoroutine = null;
		}
	}
}
