﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouristMgr : MonoBehaviour {

    public BurnedScript burned;

    public void OnMouseDown()
    {
        burned.ChangeGuiriBurnedPosition();
    }
}
