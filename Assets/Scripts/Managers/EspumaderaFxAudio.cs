﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EspumaderaFxAudio : MonoBehaviour {

	public AudioSource audioSource;
	public AudioClip wooshAudio;

	public void OnWoosh()
	{
		audioSource.Stop();
		audioSource.PlayOneShot(wooshAudio);
	}
}
