﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AftersunMgr : MonoBehaviour
{

    public GameObject _aftersunPrefab;

    public int row = 0;
    private int aftersunNumber = 0;

    private const string AFTERSUNS = "Aftersun";
	public Text numAftersunsText;
	public Button aftersunButton;

	private void Start()
	{
		if (PlayerPrefs.HasKey(AFTERSUNS))
		{
			aftersunNumber = PlayerPrefs.GetInt(AFTERSUNS);
		}
		else
		{
			aftersunNumber = 0;
		}
		aftersunButton.interactable = !(aftersunNumber == 0);
		numAftersunsText.text = "x" + aftersunNumber;
	}

	public void OnClickAftersun()
    {

        if (aftersunNumber > 0)
        {
            print("pulsamos aftersun");

            GameObject aftersun = Instantiate(_aftersunPrefab, GameMgr.Instance.grid.transform);
            aftersun.transform.localScale = Vector3.one;
            aftersun.transform.localPosition = Vector3.zero + (transform.right * 2.7f) + (transform.up * (-0.3f));
            aftersun.GetComponent<SpriteRenderer>().sortingOrder = 60;
            aftersun.transform.parent = GameMgr.Instance.grid.transform.FindChild("PowerUps");

            aftersunNumber--;
			PlayerPrefs.SetInt(AFTERSUNS, aftersunNumber);
        }

		aftersunButton.interactable = !(aftersunNumber == 0);
		numAftersunsText.text = "x" + aftersunNumber;
	}

    public void AddAftersun()
    {

		if (PlayerPrefs.HasKey(AFTERSUNS))
		{
			aftersunNumber = PlayerPrefs.GetInt(AFTERSUNS);
		}
		aftersunNumber++;
		aftersunButton.interactable = !(aftersunNumber == 0);
		numAftersunsText.text = "x" + aftersunNumber;
		PlayerPrefs.SetInt(AFTERSUNS, aftersunNumber);

    }
}
