﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMgr : MonoBehaviour {


    private Stack<string> m_stackScenes = new Stack<string>();
    private Dictionary<string, GameObject> m_desactiveGameObject = new Dictionary<string, GameObject>();

    protected void Awake()
    {
        //Para evitar que se destruya entre escenas.
        DontDestroyOnLoad(this);
        //Almacenamos la escena en la pila.
        m_stackScenes.Push(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Pushs the scene: Permite apilar escenas unas sobre otras. Es util para mantener todo el contenido en una misma escena y poder
    /// volver rapido a la escena anterior sin necesidad de volver a cargarla.
    /// </summary>
    /// <returns>
    /// La AsyncOperation que nos permite saber si dicha escena esta siendo cargada de forma asincrona
    /// </returns>
    /// <param name='sceneName'>
    /// Scene name: Nombre de la escena que queremos apilar.
    /// </param>
    /// <param name='asyn'>
    /// Asyn: parametro que determina si la carga sera sincrona o asincrona...
    /// </param>
    public void PushScene(string sceneName, bool asyn = false, bool suspend = true)
    {
        //Suspendemos la escena actual.
        // Si no hay escena que suspender (es decir apilamos la primera escena, no la suspendemos.
        if (m_stackScenes.Count > 0)
        {
            string current = m_stackScenes.Peek();
            if (suspend)
                SuspendScene(current);
            else
                PauseTime();
        }

        //Si ya tenemos la escena cargada de antes, pero esta desactivada, no la cargamos, simplemente la activamos...
        if (m_desactiveGameObject.ContainsKey(sceneName))
        {
            //Leemos del diccionario m_desactiveGameObject el nombre de la escena  para obtenerla y apilarla de nuevo usando StoreLevelInfoInStack
            //Activamos la escena y eliminamos la escena de las escenas desactivadas m_desactiveGameObject
            GameObject goScene = m_desactiveGameObject[sceneName];
            m_stackScenes.Push(sceneName);
            goScene.SetActive(true);
            m_desactiveGameObject.Remove(sceneName);
        }
        else
        {
            //Carga de la escena
            if (asyn)
            {
                //lanzamos la corrutina LoadingAdditiveAsync pasándole como parámetro LoadingAdditiveAsyncParam
                StartCoroutine("LoadingAdditiveAsync", new LoadingAdditiveAsyncParam(sceneName, true));
            }
            else
            {
                SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
                m_stackScenes.Push(sceneName);
            }
        }
    }

    protected struct LoadingAdditiveAsyncParam
    {
        public string sceneName;
        public bool inStack;
        public LoadingAdditiveAsyncParam(string a_s, bool a_i)
        {
            sceneName = a_s;
            inStack = a_i;
        }
    }

    protected void SuspendScene(string sceneName)
    { 
        GameObject root = GameObject.Find(sceneName);
        if (!m_desactiveGameObject.ContainsKey(sceneName))
            m_desactiveGameObject.Add(sceneName, root);
        else
            m_desactiveGameObject[sceneName] = root;
        root.SetActive(false);
    }

    protected float timeScale;

    protected void PauseTime()
    {
        timeScale = Time.timeScale;
        Time.timeScale = 0.0f;
    }

    protected void ReanudeTime()
    {
        if (timeScale != 0.0f)
        {
            Time.timeScale = timeScale;
            timeScale = 0.0f;
        }
        else
            Time.timeScale = 1.0f;
    }

    /// <summary>
    /// Returns the scene. Vuelve a la escena anterior.
    /// </summary>
    /// <param name='clearCurrentScene'>
    /// Clear current scene.
    /// </param>
    public void ReturnScene(bool clearCurrentScene)
    {

        if (m_stackScenes.Count < 1)
        {
            Debug.Log("No hay ninguna escena en la pila");
            return;
        }

        //Desactivamos la escena actual
        string current = m_stackScenes.Pop();
        GameObject root = GameObject.Find(current);
        m_desactiveGameObject.Add(current, root);
        root.SetActive(false);

        string previousScene = m_stackScenes.Peek();
        BackToLifeScene(previousScene);
    }

    public void BackToLifeScene(string sceneName)
    {
        if (m_desactiveGameObject.ContainsKey(sceneName))
        {
            GameObject root = m_desactiveGameObject[sceneName];
			while(root==null)
				root = m_desactiveGameObject[sceneName];
			m_desactiveGameObject.Remove(sceneName);
            root.SetActive(true);
        } else
        {
            ReanudeTime();
        }

    }

    /// <summary>
    /// Changes the scene: Cambia la escena actual por otra nueva. La escena anterior se elimina.
    /// </summary>
    /// <param name='scene'>
    /// SceneName: El nombre de la escena a cargar
    /// </param>
    /// <param name='next'>
    /// Next. Nombre de la siguiente escena a cargar.... Si queremos hacer una pantalla de carga, por ejemplo
    /// </param>
    public void ChangeScene(string sceneName, string next = "")
    {
        //Eliminamos la pila de escenas ya que vamos a renovar completamente toda la escena.
        m_stackScenes.Clear();
		
		//Vaciamos la lista de elementos desactivados.
		m_desactiveGameObject.Clear();
        //Cargamso la escena y la almacenamos en la cima de la pila usando StoreLevelInfoInStack
        SceneManager.LoadScene(sceneName);
        m_stackScenes.Push(sceneName);
		ReanudeTime();

	}

	/// <summary>
	/// Changes the scene: Cambia la escena actual por otra nueva. La escena anterior se elimina.
	/// </summary>
	/// <param name='scene'>
	/// SceneName: El nombre de la escena a cargar
	/// </param>
	/// <param name='next'>
	/// Next. Nombre de la siguiente escena a cargar.... Si queremos hacer una pantalla de carga, por ejemplo
	/// </param>
	public void ChangeSceneAsync(string sceneName)
	{
			//Eliminamos la pila de escenas ya que vamos a renovar completamente toda la escena.
			m_stackScenes.Clear();

			//Vaciamos la lista de elementos desactivados.
			m_desactiveGameObject.Clear();
			//Cargamso la escena y la almacenamos en la cima de la pila usando StoreLevelInfoInStack
			AsyncOperation async = SceneManager.LoadSceneAsync(sceneName);
			m_stackScenes.Push(sceneName);
			ReanudeTime();
	}

	public void PreloadScene(string sceneName)
	{
		StartCoroutine(PreloadSceneCoroutine(sceneName));
	}

	public IEnumerator PreloadSceneCoroutine(string sceneName)
	{
		//Cargamos la escena asincronamente
		AsyncOperation async = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

		//Esperamos hasta que este cargada.
		while (!async.isDone) yield return null;

		//Una vez cargada la desactivamos
		GameObject root = GameObject.Find(sceneName);
		m_desactiveGameObject.Add(sceneName, root);
		root.SetActive(false);
	}

	public void ChangeToPreloadScene(string sceneName)
	{
		if (m_desactiveGameObject.ContainsKey(sceneName))
		{
			//Quitamos la escena actual
			SceneManager.UnloadSceneAsync(m_stackScenes.Pop());

			//Cargamos la escena precargada
			GameObject root = m_desactiveGameObject[sceneName];
			while (root == null)
				root = m_desactiveGameObject[sceneName];
			m_desactiveGameObject.Remove(sceneName);
			root.SetActive(true);
			m_stackScenes.Push(sceneName);
		}
	}

	public void CleanScenes()
	{
		//Eliminamos la pila de escenas ya que vamos a renovar completamente toda la escena.
		m_stackScenes.Clear();

		//Vaciamos la lista de elementos desactivados.
		m_desactiveGameObject.Clear();
	}

	public string GetCurrentSceneName()
    {
        return m_stackScenes.Peek();
    }
}
