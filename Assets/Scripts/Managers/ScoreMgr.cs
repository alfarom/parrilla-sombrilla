﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreMgr : MonoBehaviour {

	private const string HIGH_SCORE = "HighScore";

	public int score;
	public Text scoreText;
	public bool isNewHighScore;

	private int bestScore;
	private Coroutine scoreCoroutine;

	// Use this for initialization
	void Start () {

		if (PlayerPrefs.HasKey(HIGH_SCORE))
		{
			bestScore = PlayerPrefs.GetInt(HIGH_SCORE);
			PlayerPrefs.DeleteAll();
		}
		else
		{
			bestScore = 0;
		}
		score = 0;
		scoreText.text = score.ToString();
		isNewHighScore = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void AddScore(int points)
	{
		if (scoreCoroutine == null)
			scoreCoroutine = StartCoroutine("SetTotalPoints",points);

		score += points;

		//Almacenamos la mejor puntuación con un combo
		if (score > bestScore)
		{
			bestScore = score;
			PlayerPrefs.SetInt(HIGH_SCORE,score);
			isNewHighScore = true;
		}
	}


	IEnumerator SetTotalPoints(int points)
	{
		int initialPoints = score;
		int targetPoints = initialPoints + points;

		while (initialPoints < targetPoints)
		{
			initialPoints += 10;
			scoreText.text = initialPoints.ToString();
			yield return null;

		}

		scoreText.text = targetPoints.ToString();
		scoreCoroutine = null;
	}

}
