﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UmbrellaMgr : MonoBehaviour {

    public GameObject _umbrellaPrefab;
    private float row = 0f;
    private float column = 0f;
    public float timeUmbrella = 0f;

    private int umbrellaNumber = 0;

    private const string UMBRELLAS = "Umbrella";

	public Text numUmbrellasText;
	public Button umbrellaButton;

	private void Start()
	{
		if (PlayerPrefs.HasKey(UMBRELLAS))
		{
			umbrellaNumber = PlayerPrefs.GetInt(UMBRELLAS);
		}
		else
		{
			umbrellaNumber = 0;	
		}
		umbrellaButton.interactable = !(umbrellaNumber == 0);
		numUmbrellasText.text = "x" + umbrellaNumber;
	}

	public void OnClickUmbrella ()
    {

		if (umbrellaNumber > 0)
        {
            print("pulsamos umbrella");
            row = Random.Range(0, GameMgr.Instance.grid.levels - 1) + 0.5f;
            column = Random.Range(-GameMgr.Instance.grid.levels + 1, GameMgr.Instance.grid.levels - 1) + 0.5f;

            GameObject umbrella = Instantiate(_umbrellaPrefab, GameMgr.Instance.grid.transform);
            umbrella.transform.localScale = Vector3.one;
            umbrella.transform.localPosition = Vector3.zero + (transform.right * column) + (transform.up * row);
            umbrella.GetComponent<Umbrella>().row = row;
            umbrella.GetComponent<Umbrella>().column = column;
            umbrella.GetComponent<Umbrella>().timeUmbrella = timeUmbrella;
            umbrella.GetComponent<SpriteRenderer>().sortingOrder = 50;
            umbrella.transform.parent = GameMgr.Instance.grid.transform.FindChild("PowerUps");

            umbrellaNumber--;
			numUmbrellasText.text = "x" + umbrellaNumber;
			umbrellaButton.interactable = !(umbrellaNumber == 0);

			PlayerPrefs.SetInt(UMBRELLAS, umbrellaNumber);
        }
        else
        {
			umbrellaButton.interactable = !(umbrellaNumber == 0);
			numUmbrellasText.text = "x" + umbrellaNumber;
			print("No hay umbrellas");
        }
	}

    public void AddUmbrella()
    {
        if (PlayerPrefs.HasKey(UMBRELLAS))
        {
            umbrellaNumber = PlayerPrefs.GetInt(UMBRELLAS);
        }
		umbrellaNumber++;
		umbrellaButton.interactable = !(umbrellaNumber == 0);
		numUmbrellasText.text = "x" + umbrellaNumber;
		PlayerPrefs.SetInt(UMBRELLAS, umbrellaNumber);
    }
}
