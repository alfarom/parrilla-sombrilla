﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverMgr : MonoBehaviour {

	public GameObject newHighScore;
	public Text score;
	public GameObject yourHighScore;
	public Text yourscore;

	private void Start()
	{
		if(GameMgr.Instance.scoreMgr.isNewHighScore){
			newHighScore.SetActive(true);
			score.text = GameMgr.Instance.scoreMgr.score.ToString();
		}else
		{
			yourHighScore.SetActive(true);
			yourscore.text = GameMgr.Instance.scoreMgr.score.ToString();
		}
	}
}
