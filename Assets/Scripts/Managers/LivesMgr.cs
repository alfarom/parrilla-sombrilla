﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivesMgr : MonoBehaviour {

	public Image[] images;
	public Sprite burnedLive;
	public int lives = 3;

	public void UnaVidaMenos()
	{
		if (lives <= 0)
		{
			return;
		}
		lives--;
		images[lives].sprite = burnedLive;
		if (lives == 0)
		{
			Invoke("ChangeScene", 1.5f);
			return;
		}
	}

	private void ChangeScene()
	{
		GameMgr.Instance.sceneMgr.PushScene("GameOver");
	}

}
