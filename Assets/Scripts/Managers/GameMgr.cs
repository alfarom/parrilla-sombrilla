﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameMgr : MonoBehaviour {
	#region Gestion del singleton

	private static GameMgr _instance;

	public static GameMgr Instance
	{
		get
		{
			if (_instance == null)
			{
				Debug.Log("GameMgr no inicializado.");
				return null;
			}
			return _instance;
		}
	}

	void Awake()
	{
		if (_instance != null)
		{
			Debug.Log("Hay dos componentes en la e..");
			Destroy(this.gameObject);
			return;
		}
		_instance = this;
		DontDestroyOnLoad(gameObject);
	}

	void OnDestroy()
	{
		if (_instance == this)
		{
			_instance = null;
		}
	}

	#endregion

	#region Variables públicas

    public GridController grid;
    public GameObject[] spawnPoints;
    public GameObject spawnPointsPosition;
    public CameraController camera;
	public LivesMgr livesMgr;
	public SceneMgr sceneMgr;
    public SpawnerMgr spawnerMgr;
    public GameObject background;
	public ScoreMgr scoreMgr;
	#endregion

	public void DestroyGameMgr()
	{
		Destroy(gameObject);
	}
}
