﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouristMovement : MonoBehaviour {

    public float Velocidad;
	public AnimationComponent animationComponent;
	private float distancia = 0f;

    public GameObject Grid;
    private Vector2 CeldaObjetivo = new Vector2();

    void Start()
    {
        SetTargetPosition();
    }
    
    // Update is called once per frame
	void Update ()
    {
        MoveToTowel();
	}

    public void SetTargetPosition()
    {
        CeldaObjetivo = this.transform.parent.transform.position;

		Vector2 direction = (Vector3)CeldaObjetivo - transform.position;
		direction.Normalize();

		transform.rotation = Quaternion.Euler(new Vector3(0,0, Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90));
	}

    void MoveToTowel()
    {
        #region Movimiento hacia el objetivo
        Vector2 dir = new Vector2();
        dir = CeldaObjetivo - (Vector2)transform.position;

        dir.Normalize();
        distancia = (CeldaObjetivo - (Vector2)transform.position).magnitude;

        if (distancia > 0.1f)
        {
			animationComponent.ResetFlop();
			transform.position = transform.position + (Vector3)dir * Velocidad * Time.deltaTime;
		}
        else if (this.transform.parent.tag == "SpawnPoint")
        {
            Destroy(gameObject);
        }
		else if (distancia <= 0.1f)
		{
			transform.rotation = Quaternion.identity;
			animationComponent.Flop();
		}
		#endregion
	}


}
