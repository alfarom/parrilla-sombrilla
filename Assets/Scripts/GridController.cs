﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridController : MonoBehaviour {

	private const int INITIAL_LEVELS = 3;

	public int levels = 0;
    private int cellNumber = 0;
    private int posibilidad = 0;
    private int row = 0;
    private List<GameObject> freeCells = new List<GameObject>();

    public GameObject[] _cellPrefabs;
    public GameObject _sandPrefab;
    public GameObject _seaPrefab;
    public GameObject _coastPrefab;

	public void Start()
	{
		for(levels = 0; levels < INITIAL_LEVELS;)
		{
			AddNewLevel();
		}
	}

	public void AddNewLevel()
	{
		for (row = 0; row <= levels; row++)
		{
			if (row == levels)
			{
				for (int column = -levels; column <= levels; column++)
				{
					//Toallas
                    GameObject towel = Instantiate(_cellPrefabs[Random.Range(0, _cellPrefabs.Length)], transform);
                    towel.transform.localScale = Vector3.one;
                    towel.transform.localPosition = Vector3.zero + (transform.right * column) + (transform.up * row);
                    towel.GetComponent<Cell>().row = row;
                    towel.GetComponent<Cell>().column = column;
                }

                for (int column = -levels - 3; column <= levels + 3; column++)
                {
					//Arena
                    GameObject sand = Instantiate(_sandPrefab, transform);
                    sand.transform.localScale = Vector3.one;
                    sand.transform.localPosition = Vector3.zero + (transform.right * column) + (transform.up * row);
                    sand.GetComponent<Sand>().row = row;
                    sand.GetComponent<Sand>().column = column;
                    sand.GetComponent<SpriteRenderer>().sortingOrder = -100;
                    sand.transform.parent = GameMgr.Instance.background.transform.FindChild("Sands");
				}
			}
			else
			{
				//Toallas
                GameObject leftCellTowel = Instantiate(_cellPrefabs[Random.Range(0, _cellPrefabs.Length)], transform);
                leftCellTowel.transform.localScale = Vector3.one;
                leftCellTowel.transform.localPosition = Vector3.zero + (transform.right * -levels) + (transform.up * row);
                leftCellTowel.GetComponent<Cell>().row = row;
                leftCellTowel.GetComponent<Cell>().column = -levels;

                GameObject rightCellTowel = Instantiate(_cellPrefabs[Random.Range(0, _cellPrefabs.Length)], transform);
                rightCellTowel.transform.localScale = Vector3.one;
                rightCellTowel.transform.localPosition = Vector3.zero + (transform.right * levels) + (transform.up * row);
                rightCellTowel.GetComponent<Cell>().row = row;
                rightCellTowel.GetComponent<Cell>().column = levels;

                //Arena
                GameObject leftCellSand = Instantiate(_sandPrefab, transform);
                leftCellSand.transform.localScale = Vector3.one;
                leftCellSand.transform.localPosition = Vector3.zero + (transform.right * -(levels + 3)) + (transform.up * row);
                leftCellSand.GetComponent<Sand>().row = row;
                leftCellSand.GetComponent<Sand>().column = -levels;
                leftCellSand.GetComponent<SpriteRenderer>().sortingOrder = -100;
                leftCellSand.transform.parent = GameMgr.Instance.background.transform.FindChild("Sands");

                GameObject rightCellSand = Instantiate(_sandPrefab, transform);
                rightCellSand.transform.localScale = Vector3.one;
                rightCellSand.transform.localPosition = Vector3.zero + (transform.right * (levels + 3)) + (transform.up * row);
                rightCellSand.GetComponent<Sand>().row = row;
                rightCellSand.GetComponent<Sand>().column = levels;
                rightCellSand.GetComponent<SpriteRenderer>().sortingOrder = -100;
                rightCellSand.transform.parent = GameMgr.Instance.background.transform.FindChild("Sands");
			}
		}

        if (row == (levels + 1))
        {
			for (var count = 0; count < 3; count++)
			{
				row = row + count;
				for (int column = -levels - 3; column <= levels + 3; column++)
				{
					//Arena
					GameObject sand = Instantiate(_sandPrefab, transform);
					sand.transform.localScale = Vector3.one;
					sand.transform.localPosition = Vector3.zero + (transform.right * column) + (transform.up * row);
					sand.GetComponent<Sand>().row = row;
					sand.GetComponent<Sand>().column = column;
					sand.GetComponent<SpriteRenderer>().sortingOrder = -100;
					sand.transform.parent = GameMgr.Instance.background.transform.FindChild("Sands");
				}
			}

		}

        for (int column = -levels - 3; column <= levels + 3; column++)
        {
            //Costa
            GameObject coast = Instantiate(_coastPrefab, transform);
            coast.transform.localScale = Vector3.one;
            coast.transform.localPosition = Vector3.zero + (transform.right * column) + (transform.up * (-1));
            coast.GetComponent<Coast>().row = (-1);
            coast.GetComponent<Coast>().column = column;
            coast.GetComponent<SpriteRenderer>().sortingOrder = -100;
            coast.transform.parent = GameMgr.Instance.background.transform.FindChild("Coasts");

            //Mar
            GameObject sea = Instantiate(_seaPrefab, transform);
            sea.transform.localScale = Vector3.one;
            sea.transform.localPosition = Vector3.zero + (transform.right * column) + (transform.up * (-1));
            sea.GetComponent<Sea>().row = (-1);
            sea.GetComponent<Sea>().column = column;
            sea.GetComponent<SpriteRenderer>().sortingOrder = -150;
            sea.transform.parent = GameMgr.Instance.background.transform.FindChild("Seas");
        }

		levels++;
	}

    public GameObject GetFreeCell()
    {
        cellNumber = this.transform.childCount;
        freeCells = new List<GameObject>();

        for (var i = 0; i < cellNumber; i++)
        {
            if (transform.GetChild(i).tag == "Cell")
            {
            if (transform.GetChild(i).GetComponent<Cell>().isFree)
                {
                    freeCells.Add(transform.GetChild(i).gameObject);
                }
            }
           
            posibilidad = Random.Range(0, freeCells.Count);

        }
        return freeCells[posibilidad];
    }
}
