﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AftersunBubble : MonoBehaviour
{
    private Coroutine coroutine;
    
    public float speed = 0f;

    public GameObject _aftersunSnowPrefab;

    public void Update()
    {
        gameObject.transform.position = gameObject.transform.position + Vector3.up * speed * Time.deltaTime;

        //Si sale de la pantalla, instanciamos la lluvia y destruimos este objeto
        if (gameObject.transform.position.y > 7)
        {
            GameObject aftersunSnow = Instantiate(_aftersunSnowPrefab, GameMgr.Instance.grid.transform);
            aftersunSnow.transform.localScale = Vector3.one;
            aftersunSnow.transform.localPosition = Vector3.zero + Vector3.up * 5;
            aftersunSnow.transform.parent = GameMgr.Instance.grid.transform.FindChild("PowerUps");

            Destroy(gameObject);
        }
    }

    
}
