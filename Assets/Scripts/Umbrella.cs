﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Umbrella : MonoBehaviour
{

    public float row { get; set; }
    public float column { get; set; }
    public float timeUmbrella { get; set; }

    public void Update()
    {
        if (this.timeUmbrella <= 0)
        {
            Destroy(gameObject);

            //Recorremos los hijos del grid
            for (var i = 0; i < GameMgr.Instance.grid.transform.childCount; i++)
            {
                //Comprobamos si es una celda y está en las casillas adyacentes
                if ((GameMgr.Instance.grid.transform.GetChild(i).name.Contains("Cell")) &&
                    (GameMgr.Instance.grid.transform.GetChild(i).GetComponent<Cell>().row == row - 0.5f ||
                    GameMgr.Instance.grid.transform.GetChild(i).GetComponent<Cell>().row == row + 0.5f) &&
                    (GameMgr.Instance.grid.transform.GetChild(i).GetComponent<Cell>().column == column - 0.5f ||
                    GameMgr.Instance.grid.transform.GetChild(i).GetComponent<Cell>().column == column + 0.5f))
                {
                    //Comprobamos si hay turista asociado a la celda
                    if (GameMgr.Instance.grid.transform.GetChild(i).childCount > 0)
                    {
                        //Comprobamos si el turista ha llegado a la toalla
                        if ((GameMgr.Instance.grid.transform.GetChild(i).GetChild(0).position - GameMgr.Instance.grid.transform.GetChild(i).position).magnitude <= 0.1f)
                        {
                            GameMgr.Instance.grid.transform.GetChild(i).GetChild(0).GetComponentInChildren<BurnedScript>().isPaused = false;
                        }
                    }
                }
            }

            return;
        }
        else
        {
            //Recorremos los hijos del grid
            for (var i = 0; i < GameMgr.Instance.grid.transform.childCount; i++)
            {
                //Comprobamos si es una celda y está en las casillas adyacentes
                if ((GameMgr.Instance.grid.transform.GetChild(i).name.Contains("Cell")) &&
                    (GameMgr.Instance.grid.transform.GetChild(i).GetComponent<Cell>().row == row - 0.5f ||
                    GameMgr.Instance.grid.transform.GetChild(i).GetComponent<Cell>().row == row + 0.5f) &&
                    (GameMgr.Instance.grid.transform.GetChild(i).GetComponent<Cell>().column == column - 0.5f ||
                    GameMgr.Instance.grid.transform.GetChild(i).GetComponent<Cell>().column == column + 0.5f))
                {
                    //Comprobamos si hay turista asociado a la celda
                    if (GameMgr.Instance.grid.transform.GetChild(i).childCount > 0)
                    {
                        //Comprobamos si el turista ha llegado a la toalla
                        if ((GameMgr.Instance.grid.transform.GetChild(i).GetChild(0).position - GameMgr.Instance.grid.transform.GetChild(i).position).magnitude <= 0.1f)
                        {
                            GameMgr.Instance.grid.transform.GetChild(i).GetChild(0).GetComponentInChildren<BurnedScript>().isPaused = true;
                        }
                    }
                }
            }
        }
        this.timeUmbrella -= Time.deltaTime;
    }
}
