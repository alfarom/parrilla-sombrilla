﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationComponent : MonoBehaviour {

	public Animator animator;
	public Animator childAnimator;

	public void DownToUp()
	{
		animator.SetTrigger("DownToUp");
		childAnimator.SetTrigger("DownToUp");
	}

	public void UpToDown()
	{
		animator.SetTrigger("UpToDown");
		childAnimator.SetTrigger("UpToDown");
	}

	public void ResetFlop()
	{
		animator.ResetTrigger("Flop");

	}

	public void Flop()
	{
		animator.SetTrigger("Flop");
	}

	public void Flip()
	{
		animator.SetTrigger("Flip");
	}

	public bool isFlipping
	{
		get { return animator.GetCurrentAnimatorStateInfo(0).IsName("DownToUp") || animator.GetCurrentAnimatorStateInfo(0).IsName("UpToDown"); }
	}
}

