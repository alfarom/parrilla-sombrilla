﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BurnedScript : MonoBehaviour {

	public SpriteRenderer guiriImage;
	public AnimationComponent animationComponent;

	public Sprite frontSprite;
	public Sprite backSprite;

	[Tooltip("Tiempo para que se añadan nuevas filas")]
	public Color normalColor;

	[Tooltip("Tiempo para que se añadan nuevas filas")]
	public Color tanColor;

	[Tooltip("Tiempo para que se añadan nuevas filas")]
	public Color burnedColor;

	[Tooltip("Tiempo para que se añadan nuevas filas")]
	public Color scorchedColor;

	public float tanTime;
	public float burnedTime;
	public float scorchedTime;

    public float speed;

    private int spawnPointsPosibilidad = 0;

	public GameObject espumaderaPrefab;

	private GuiriProperties frontProperties;
	private GuiriProperties backProperties;
	private GuiriPositionStates currentPosition;

	private Coroutine coroutine;
	public GuiriFXAudio fxAudio;
	public GameObject textDamage;

    public bool isPaused = false;

	public GameObject burnedAdvice;

	public GameObject scorchedParticles;


	public void Start()
	{
		InitializeProperties();
		currentPosition = GuiriPositionStates.BACK;
		coroutine = StartCoroutine(SetGuiriColor(tanColor, tanTime));
	}

	private void InitializeProperties()
	{
		GuiriProperties newProperties = new GuiriProperties();
		newProperties.currentColor = Color.white;
		newProperties.time = 0.0f;
		newProperties.state = GuiriStates.NORMAL;
		SaveProperties(GuiriPositionStates.BACK, newProperties);
		SaveProperties(GuiriPositionStates.FRONT, newProperties);
	}


	IEnumerator SetGuiriColor(Color targetColor, float time)
	{
		//Recuperamos las propiedas
		GuiriProperties currentProperties = GetCurrentProperties(currentPosition);

		//Calculamos el tiempo que le queda al estado
		time = time - currentProperties.time;

		//Calculamos los delta de cada color
		float deltaColorR = (targetColor.r - guiriImage.color.r) / (time * speed);
		float deltaColorG = (targetColor.g - guiriImage.color.g) / (time * speed);
		float deltaColorB = (targetColor.b - guiriImage.color.b) / (time * speed);
		float deltaColorA = (targetColor.a - guiriImage.color.a) / (time * speed);

		//Le ponemos la guiri el color actual que tiene
		Color color = currentProperties.currentColor;

		//Inicializamos el timer y el tiempo de espera
		float timer = 0.0f;
		float waitTime = 1 / speed;

		while (timer<time)
		{

            while (isPaused)
            {
                yield return null;
            }
			color += new Color(deltaColorR, deltaColorG, deltaColorB, deltaColorA);
			guiriImage.color = color;
			yield return new WaitForSeconds(waitTime);
			timer +=(waitTime);

			//Guardamos las propiedades actuales
			GuiriProperties newProperties = new GuiriProperties();
			newProperties.currentColor = color;
			newProperties.time = timer;
			newProperties.state = GetCurrentProperties(currentPosition).state;
			SaveProperties(currentPosition, newProperties);
		}

		//Guardamos las propiedades antes de hacer el cambio de estado del guiri
		GuiriProperties newCurrentProperties = new GuiriProperties();
		newCurrentProperties.currentColor = color;
		newCurrentProperties.time = 0.0f;
		GuiriStates currentState = GetCurrentProperties(currentPosition).state;
		GuiriStates nextState = currentState+1;
		newCurrentProperties.state = nextState;
		SaveProperties(currentPosition, newCurrentProperties);

		if (newCurrentProperties.state.Equals(GuiriStates.SCORCHED))
		{
			fxAudio.OnGuiriScorched();
			GameObject go = Instantiate(scorchedParticles, transform.parent);
			go.transform.localScale = Vector3.one;
			go.transform.localPosition = Vector3.zero;
			go.transform.parent = null;

			//Lanzar que se destruya el guiri
			GameMgr.Instance.livesMgr.UnaVidaMenos();
			Destroy(transform.parent.gameObject);
		}
		else
		{
			if (newCurrentProperties.state.Equals(GuiriStates.TAN))
			{
				fxAudio.OnGuiriToThePoint();
			}else if (newCurrentProperties.state.Equals(GuiriStates.BURNED))
			{
				burnedAdvice.SetActive(true);
			}

			//Pasamos al siguiente estado
			coroutine = StartCoroutine(SetGuiriColor(GetTargetColor(newCurrentProperties.state), GetTargetTime(newCurrentProperties.state)));
		}
	}

	public void ChangeGuiriBurnedPosition()
	{
		//Paramos la coroutine
		if (coroutine != null)
		{
			StopCoroutine(coroutine);
		}

		if (animationComponent.isFlipping) return;

		//Comprobamos si el guiri ya esta hecho
		if (CheckState()) return;

		transform.parent.GetComponent<SpriteRenderer>().enabled = false;

		//Le ponemos el color blanco de normal
		guiriImage.color = Color.white;
		burnedAdvice.SetActive(false);

		GameObject go = Instantiate(espumaderaPrefab, transform.parent);
		go.transform.localScale = Vector3.one;
		go.transform.localPosition = Vector3.zero;
		//Cambiamos la posicion del guiri
		if (currentPosition.Equals(GuiriPositionStates.BACK))
		{
			currentPosition = GuiriPositionStates.FRONT;
			animationComponent.DownToUp();
		}
		else
		{
			currentPosition = GuiriPositionStates.BACK;
			animationComponent.UpToDown();
		}

		Invoke("ActiveColor", 0.3f);
	}

	public void ActiveColor()
	{
		//lanzamos animacion de volteo
		GuiriProperties currentProperties = GetCurrentProperties(currentPosition);
		guiriImage.color = currentProperties.currentColor;

		if (currentProperties.state.Equals(GuiriStates.BURNED))
		{
			burnedAdvice.SetActive(true);
		}

		//Lanzar esto al final de la animacion de flip
		coroutine = StartCoroutine(SetGuiriColor(GetTargetColor(currentProperties.state), GetTargetTime(currentProperties.state)));

	}

	private bool CheckState()
	{
		if (!GetCurrentProperties(GuiriPositionStates.BACK).state.Equals(GuiriStates.NORMAL) 
			&& !GetCurrentProperties(GuiriPositionStates.FRONT).state.Equals(GuiriStates.NORMAL))
		{
			burnedAdvice.SetActive(false);
			PlaySoundCheckStates();
			SetScore();
			animationComponent.Flip();
			guiriImage.color = Color.white;
			transform.parent.GetComponent<SpriteRenderer>().enabled = false;
            DesactiveChildImage();
			Invoke("MoveToSpawnPoint", 0.5f);
			return true;
		}
		return false;
	}

	private void SetScore()
	{
		int score = 0;

		if (GetCurrentProperties(GuiriPositionStates.BACK).state.Equals(GuiriStates.TAN))
		{
			score += 50;
		}
		else if (GetCurrentProperties(GuiriPositionStates.BACK).state.Equals(GuiriStates.BURNED))
		{
			score -= 10;
		}

		if (GetCurrentProperties(GuiriPositionStates.FRONT).state.Equals(GuiriStates.TAN))
		{
			score += 50;
		}
		else if (GetCurrentProperties(GuiriPositionStates.FRONT).state.Equals(GuiriStates.BURNED))
		{
			score -= 10;
		}

		GameMgr.Instance.scoreMgr.AddScore(score);
		GameObject textDamageGO = Instantiate(textDamage, transform.parent, true);
		textDamageGO.transform.localPosition = Vector3.zero;
		textDamageGO.transform.localScale = Vector3.one;
		textDamageGO.transform.SetParent(null);
		textDamageGO.GetComponentInChildren<Text>().text = score.ToString();
		StartCoroutine("DestroyTextDamage", textDamageGO);
	}

	protected IEnumerator DestroyTextDamage(GameObject textDamage)
	{
		yield return new WaitForSeconds(2);
		Destroy(textDamage);
	}

	private void PlaySoundCheckStates()
	{
		if (GetCurrentProperties(GuiriPositionStates.BACK).state.Equals(GuiriStates.TAN)
			&& GetCurrentProperties(GuiriPositionStates.FRONT).state.Equals(GuiriStates.TAN))
		{
			fxAudio.OnTanGuiri();
		}
		else if ((GetCurrentProperties(GuiriPositionStates.BACK).state.Equals(GuiriStates.TAN)
		   && GetCurrentProperties(GuiriPositionStates.FRONT).state.Equals(GuiriStates.BURNED))
		   	|| (GetCurrentProperties(GuiriPositionStates.BACK).state.Equals(GuiriStates.BURNED)
			&& GetCurrentProperties(GuiriPositionStates.FRONT).state.Equals(GuiriStates.TAN)))
		{
			fxAudio.OnBurnedGuiri();
		}
		else if (GetCurrentProperties(GuiriPositionStates.BACK).state.Equals(GuiriStates.BURNED)
		   && GetCurrentProperties(GuiriPositionStates.FRONT).state.Equals(GuiriStates.BURNED))
		{
			fxAudio.OnScorchedGuiri();
		}
	}

	public void MoveToSpawnPoint()
	{
		spawnPointsPosibilidad = Random.Range(0, GameMgr.Instance.spawnPoints.Length);
		animationComponent.transform.parent = GameMgr.Instance.spawnPoints[spawnPointsPosibilidad].transform;
		animationComponent.gameObject.GetComponent<TouristMovement>().SetTargetPosition();
	}

	public void SaveProperties(GuiriPositionStates currentPosition, GuiriProperties newProperties)
	{
		switch (currentPosition)
		{
			case GuiriPositionStates.BACK:
				backProperties = newProperties;
				break;
			case GuiriPositionStates.FRONT:
				frontProperties = newProperties;
				break;
		}
	}

	public GuiriProperties GetCurrentProperties(GuiriPositionStates currentPosition)
	{
		switch (currentPosition)
		{
			case GuiriPositionStates.BACK:
				return backProperties;
			case GuiriPositionStates.FRONT:
				return frontProperties;
		}
		return new GuiriProperties();
	}

	public Color GetTargetColor(GuiriStates currentState)
	{
		switch (currentState)
		{
			case GuiriStates.NORMAL:
				return tanColor;
			case GuiriStates.TAN:
				return burnedColor;
			case GuiriStates.BURNED:
				return scorchedColor;
			case GuiriStates.SCORCHED:
				return scorchedColor;
		}
		return burnedColor;
	}

	public float GetTargetTime(GuiriStates currentState)
	{
		switch (currentState)
		{
			case GuiriStates.NORMAL:
				return tanTime;
			case GuiriStates.TAN:
				return burnedTime;
			case GuiriStates.BURNED:
				return scorchedTime;
			case GuiriStates.SCORCHED:
				return scorchedTime;
		}
		return scorchedTime;
	}


	public void ActiveChildImage()
	{
		transform.GetChild(0).gameObject.SetActive(true);
	}

	public void DesactiveChildImage()
	{
		transform.GetChild(0).gameObject.SetActive(false);
	}

	public void SetFrontSprite()
	{
		guiriImage.sprite = frontSprite;
		transform.parent.GetComponent<SpriteRenderer>().enabled = true;
		transform.parent.GetComponent<SpriteRenderer>().sprite = frontSprite;
	}

	public void SetBackSprite()
	{
		guiriImage.sprite = backSprite;
		transform.parent.GetComponent<SpriteRenderer>().enabled = true;
		transform.parent.GetComponent<SpriteRenderer>().sprite = backSprite;
	}

}



public enum GuiriPositionStates
{
	FRONT, BACK
}

public enum GuiriStates
{
	NORMAL, TAN, BURNED, SCORCHED
}

public struct GuiriProperties
{
	public float time;
	public GuiriStates state;
	public Color currentColor;
}
