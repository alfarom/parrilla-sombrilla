﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClothesScript : MonoBehaviour {

	public SpriteRenderer image;
	public Sprite frontSprite;
	public Sprite backSprite;

	public BurnedScript burnedScript;

	public void SetFrontSprite()
	{
		image.sprite = frontSprite;
	}

	public void SetBackSprite()
	{
		image.sprite = backSprite;
	}
}
