﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeScenesUI : MonoBehaviour {

    public string m_nextSceneAfterTransitionScene;
    public bool m_return;

    /// <summary>
    /// El método es llamado por el sistema de eventos de la UI y se le pasa por parámetro la escena que quiere cargar. Adicionalmente peude tener establecida como variable pública
    /// en el inspector una siguiente escena a cargar, en caso de que la escena de carga sea una pantalla de carga.
    /// </summary>
    /// <param name="sceneToGo"></param>
    public void OnChangeScene(string sceneToGo)
    {
        if (m_return)
            GameMgr.Instance.sceneMgr.ReturnScene(false);
        else
            GameMgr.Instance.sceneMgr.ChangeScene(sceneToGo, m_nextSceneAfterTransitionScene);
    }

	public void PushScene(string sceneToGo)
	{
		GameMgr.Instance.sceneMgr.PushScene(sceneToGo);
	}

    /// <summary>
    /// El método es llamado por el sistema de eventos de la UI para cerrar la aplciación.
    /// </summary>
    /// <param name="quit"></param>
    public void OnQuitScene(bool quit)
    {
        if (quit)
            Application.Quit();
    }

	public void DestroyGameMgr()
	{
		GameMgr.Instance.DestroyGameMgr();
	}
}
