﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteRendererClone : MonoBehaviour {

	public SpriteRenderer source;
	public SpriteRenderer target;

	// Update is called once per frame
	void Update () {
		target.sprite = source.sprite;
	}
}
