﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aftersun : MonoBehaviour
{
    private Coroutine coroutine;
    public float waitTime = 0;

    public GameObject _aftersunBlobPrefab;
    
    public void Start()
    {
        for (var i = 0; i < GameMgr.Instance.grid.transform.childCount; i++)
        {
            //Comprobamos si es una celda y está en las casillas adyacentes
            if (GameMgr.Instance.grid.transform.GetChild(i).name.Contains("Cell"))
            {
                //Comprobamos si hay turista asociado a la celda
                if (GameMgr.Instance.grid.transform.GetChild(i).childCount > 0)
                {
                    //Comprobamos si el turista ha llegado a la toalla
                    if ((GameMgr.Instance.grid.transform.GetChild(i).GetChild(0).position - GameMgr.Instance.grid.transform.GetChild(i).position).magnitude <= 0.1f)
                    {
                        GameMgr.Instance.grid.transform.GetChild(i).GetChild(0).GetComponentInChildren<BurnedScript>().tanTime =
                            GameMgr.Instance.grid.transform.GetChild(i).GetChild(0).GetComponentInChildren<BurnedScript>().tanTime * 2;

                        GameMgr.Instance.grid.transform.GetChild(i).GetChild(0).GetComponentInChildren<BurnedScript>().burnedTime =
                            GameMgr.Instance.grid.transform.GetChild(i).GetChild(0).GetComponentInChildren<BurnedScript>().burnedTime * 2;

                        GameMgr.Instance.grid.transform.GetChild(i).GetChild(0).GetComponentInChildren<BurnedScript>().scorchedTime =
                            GameMgr.Instance.grid.transform.GetChild(i).GetChild(0).GetComponentInChildren<BurnedScript>().scorchedTime * 2;
                    }
                }
            }
        }
        coroutine = StartCoroutine(CreateBubble(waitTime));

    }

    IEnumerator CreateBubble(float waitTime)
    {
        GameObject aftersun = Instantiate(_aftersunBlobPrefab, GameMgr.Instance.grid.transform);
        aftersun.transform.localScale = Vector3.one;
        aftersun.transform.localPosition = gameObject.transform.position;
        aftersun.GetComponent<SpriteRenderer>().sortingOrder = 55;
        aftersun.transform.parent = GameMgr.Instance.grid.transform.FindChild("PowerUps");

        yield return new WaitForSeconds(waitTime);
        Destroy(gameObject);
        yield return null;
    }
}
