﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public float Velocidad;
    private float distancia = 0f;

    public Vector3 Objetivo = new Vector3();

    public bool cameraInPosition = true;

    void Update()
    {
        if (!cameraInPosition)
        {
            MoveCamera();
        }
    }

    public void MoveCamera()
    {
        #region Movimiento hacia el objetivo
        Vector3 dir = new Vector3();
        dir = Objetivo - transform.position;

        dir.Normalize();
        distancia = (Objetivo - transform.position).magnitude;

        if (distancia > 0.1f)
        {
            transform.position = transform.position + dir * Velocidad * Time.deltaTime;
        }
        else
        {
            cameraInPosition = true;
        }
        #endregion
    }
}
