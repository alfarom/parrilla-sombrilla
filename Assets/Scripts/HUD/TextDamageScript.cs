﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextDamageScript : MonoBehaviour {

	bool endX, endY, endZ;

    void Start()
    {
		transform.parent.LookAt(GameObject.FindGameObjectWithTag("MainCamera").transform);
		StartCoroutine("MoveX");
		StartCoroutine("MoveY");
        StartCoroutine("MoveZ");
		StartCoroutine("Scale");
		StartCoroutine("Alpha");
    }

    IEnumerator MoveY()
    {
		float posY = Random.Range(0.5f, 0.75f);
		float deltaY = posY / 30;

        int i = 0;
        while (i < 30)
        {
			transform.position += transform.up * deltaY;
			i++;
            yield return null;
        }

		deltaY = (1+ posY) / 80;
		i = 0;
		while (i < 80)
		{
			transform.position -= transform.up * deltaY;
			i++;
			yield return null;
		}
		
	}

    IEnumerator MoveZ()
    {
		float deltaZ = 1f / 110;

		int i = 0;
		while (i < 110)
		{
			transform.position += transform.forward * deltaZ;
			i++;
			yield return null;
		}

	}

	IEnumerator Alpha()
	{
		float alpha = 1f / 110;
		Text text = GetComponent<Text>();
		Outline outline = GetComponent<Outline>();
		Color color = text.color;
		Color outlineColor = text.color;

		int i = 0;
		while (i < 110)
		{
			color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - alpha);
			outlineColor = new Color(outline.effectColor.r, outline.effectColor.g, outline.effectColor.b, outline.effectColor.a - alpha);
			text.color= color;
			i++;
			yield return null;
		}

	}

	IEnumerator Scale()
	{
		//0.0075 sube a 0.015 y baja a 0.075
		float deltaScale = 0.0075f / 30;
		transform.localScale = Vector3.one * 0.0075f;
		int i = 0;
		while (i < 30)
		{
			transform.localScale += Vector3.one * deltaScale;
			i++;
			yield return null;
		}

		deltaScale = 0.0075f / 80;
		i = 0;
		while (i < 80)
		{
			transform.localScale -= Vector3.one * deltaScale;
			i++;
			yield return null;
		}

	}

	IEnumerator MoveX()
	{
		float posX = Random.Range(-0.5f, 0.5f);
		float deltaX = posX / 40;

		int i = 0;
		while (i < 40)
		{
			transform.position += transform.right * deltaX;
			i++;
			yield return null;
		}

	}
}
